/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 14/9/13
 * Time: 1:22 PM
 * To change this template use File | Settings | File Templates.
 */
app.controller('acct_pointsController', function ($scope, $http, $q, $location, acctService, localStorageService) {

    init();

    function init() {
        $scope.user_point = parseInt(acctService.get_points()).toFixed(0);
		if($scope.user_point < 0)
        {
            $scope.user_point = 0;

        }
        get_points_credit();
        get_points_debit();

    }

    function get_points_credit(){
        var e = localStorageService.get('user_info');
        var u;
        if (e.hasOwnProperty('email_id')){
            u = e.email_id;
        }
        else {
            u= e.user_id;
        }
        acctService.get_points_credit($http, $q, u).then(function(data){
                $scope.points_credit=  data;
                //console.log(data);
            },
            function(){
                //Display an error message
                $scope.error= error;

            });
    }
    function get_points_debit(){
        var e = localStorageService.get('user_info');
        var u;
        if (e.hasOwnProperty('email_id')){
            u = e.email_id;
        }
        else {
            u= e.user_id;
        }
        acctService.get_points_debit($http, $q, u).then(function(data){
                $scope.points_debit =  data;
                //console.log(data);
            },
            function(){
                //Display an error message
                $scope.error= error;
                $scope.loading = false;
            });
    }

});
