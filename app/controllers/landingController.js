/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 14/7/13
 * Time: 5:40 AM
 * To change this template use File | Settings | File Templates.
 */
app.controller('landingController', function ($scope, $location, $http, $q,  landingService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    init();

    function init() {
        landingService.setupLanding();
    }




});