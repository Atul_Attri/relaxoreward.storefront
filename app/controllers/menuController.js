/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 24/6/13
 * Time: 9:26 PM
 * To change this template use File | Settings | File Templates.
 */
//This controller retrieves data from the cache/cat.json and associates it with the $scope
//The $scope is ultimately bound to the menu view
app.controller('MenuController', function ($scope, $location, $window,  menuService, $http, $q, localStorageService, utilService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    init();
    var menu;
    function init() {
        console.log(utilService.get_store())
        get_all_menu();
    }
    this.isLevel2 = function(val){
        if (val == 2) {
            return true;
        }
        else{
            return false;
        }
    }
    $scope.get_menu = function(name, level){
        //console.log(menuService.get_menu(name,level));
        menuService.get_menu(name,level);
    }

    $scope.open_level1_page = function(name){
        //console.log(menuService.get_menu(name,level));
        url = root_url + "/cat1.html?cat=" + name ;
        $window.location.href = url;
    }

    // new methods//
    function get_all_menu(){
        var expiry_status;
        var last_download_date;
        var tmp_menu =localStorageService.get("menu");
        if (localStorageService.get("menu_refresh")===null)
        {
            last_download_date = ""
        }
        else{
            last_download_date =  localStorageService.get("menu_refresh");
        }
        if (tmp_menu != null){
            // check expiry of menu
            menuService.get_menu_expiry($http, $q, last_download_date).then(function(data){
                var expiry_status = data;
                if (expiry_status > 0) {
                    // get menu again
                    menuService.get_all_menu($http, $q, utilService.get_store()).then(function(data){
                            menu = data;
                            //console.log(_.filter(menu,function(m){ return m.name === "kids"}));
                            var last_refresh = new XDate();

                            localStorageService.add("menu", menu);
                            localStorageService.add("menu_refresh", last_refresh.toDateString("MMM d, yyyy"));
                            //console.log(new XDate());
                            if (get_20_menu().length !=0) {
                                //$scope.500_link = _.filter(menu,function(m){ return m.name === "500"})[0].id;
                                $scope.l2_20_menu = get_level2_20_menu();
                                $scope.l3_20_menu = get_level3_20_menu();
                            }
                            if (get_30_menu().length !=0) {
                                //$scope.1000_link = _.filter(menu,function(m){ return m.name === "1000"})[0].id;
                                $scope.l2_30_menu = get_level2_30_menu();
                                $scope.l3_30_menu = get_level3_30_menu();
                            }
                            if (get_50_menu().length !=0) {
                                //$scope.1500_link = _.filter(menu,function(m){ return m.name === "1500"})[0].id;
                                $scope.l2_50_menu = get_level2_50_menu();
                                $scope.l3_50_menu = get_level3_50_menu();
                            }
                            if (get_75_menu().length !=0) {
                                //$scope.2000_link = _.filter(menu,function(m){ return m.name === "2000"})[0].id;
                                $scope.l2_75_menu = get_level2_75_menu();
                                $scope.l3_75_menu = get_level3_75_menu();
                            }

                            if (get_100_menu().length !=0) {
                                //$scope.2500_link = _.filter(menu,function(m){ return m.name === "2500"})[0].id;
                                $scope.l2_100_menu = get_level2_100_menu();
                                $scope.l3_100_menu = get_level3_100_menu();
                            }
                            if (get_150_menu().length !=0) {
                                //$scope.3000_link = _.filter(menu,function(m){ return m.name === "3000"})[0].id;
                                $scope.l2_150_menu = get_level2_150_menu();
                                $scope.l3_150_menu = get_level3_150_menu();
                            }
                             if (get_250_menu().length !=0) {
                                //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                                $scope.l2_250_menu = get_level2_250_menu();
                                $scope.l3_250_menu = get_level3_250_menu();
                            }
                             if (get_500_menu().length !=0) {
                                //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                                $scope.l2_500_menu = get_level2_500_menu();
                                $scope.l3_500_menu = get_level3_500_menu();
                            }
                             if (get_750_menu().length !=0) {
                                //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                                $scope.l2_750_menu = get_level2_750_menu();
                                $scope.l3_750_menu = get_level3_750_menu();
                            }
                             if (get_1000_menu().length !=0) {
                                //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                                $scope.l2_1000_menu = get_level2_1000_menu();
                                $scope.l3_1000_menu = get_level3_1000_menu();
                            }
                             if (get_1500_menu().length !=0) {
                                //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                                $scope.l2_1500_menu = get_level2_1500_menu();
                                $scope.l3_1500_menu = get_level3_1500_menu();
                            }
                             if (get_2000_menu().length !=0) {
                                //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                                $scope.l2_2000_menu = get_level2_2000_menu();
                                $scope.l3_2000_menu = get_level3_2000_menu();
                            }
                             if (get_3500_menu().length !=0) {
                                //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                                $scope.l2_3500_menu = get_level2_3500_menu();
                                $scope.l3_3500_menu = get_level3_3500_menu();
                            }

                        },
                        function(){
                            //Display an error message
                            $scope.error= error;
                        });

                }
                else {
                    //

                    menu = localStorageService.get("menu")   ;

                    if (get_20_menu().length !=0) {
                        $scope.l2_20_menu = get_level2_20_menu();
                        $scope.l3_20_menu = get_level3_20_menu();
                    }
                    if (get_30_menu().length !=0) {
                        $scope.l2_30_menu = get_level2_30_menu();
                        $scope.l3_30_menu = get_level3_30_menu();
                    }
                    if (get_50_menu().length !=0) {
                        $scope.l2_50_menu = get_level2_50_menu();
                        $scope.l3_50_menu = get_level3_50_menu();
                    }
                    if (get_75_menu().length !=0) {
                        $scope.l2_75_menu = get_level2_75_menu();
                        $scope.l3_75_menu = get_level3_75_menu();
                    }
                    if (get_100_menu().length !=0) {
                        $scope.l2_100_menu = get_level2_100_menu();
                        $scope.l3_100_menu = get_level3_100_menu();
                    }
                    if (get_150_menu().length !=0) {
                        $scope.l2_150_menu = get_level2_150_menu();
                        $scope.l3_150_menu = get_level3_150_menu();
                    }
                    if (get_250_menu().length !=0) {
                        $scope.l2_250_menu = get_level2_250_menu();
                        $scope.l3_250_menu = get_level3_250_menu();
                    }
                    if (get_500_menu().length !=0) {
                        $scope.l2_500_menu = get_level2_500_menu();
                        $scope.l3_500_menu = get_level3_500_menu();
                    }
                     if (get_750_menu().length !=0) {
                        $scope.l2_750_menu = get_level2_750_menu();
                        $scope.l3_750_menu = get_level3_750_menu();
                    }
                     if (get_1000_menu().length !=0) {
                        $scope.l2_1000_menu = get_level2_1000_menu();
                        $scope.l3_1000_menu = get_level3_1000_menu();
                    }
                    if (get_1500_menu().length !=0) {
                        $scope.l2_1500_menu = get_level2_1500_menu();
                        $scope.l3_1500_menu = get_level3_1500_menu();
                    }
                    if (get_2000_menu().length !=0) {
                        $scope.l2_2000_menu = get_level2_2000_menu();
                        $scope.l3_2000_menu = get_level3_2000_menu();
                    }
                    if (get_3500_menu().length !=0) {
                        $scope.l2_3500_menu = get_level2_3500_menu();
                        $scope.l3_3500_menu = get_level3_3500_menu();
                    }

                }

            });

        }
        else {
                //console.log("making call to get all menu");
                var company  =   localStorageService.get("user_info").company;
                menuService.get_all_menu($http, $q,company).then(function(data){
                menu = data;
                //console.log(_.filter(menu,function(m){ return m.name === "kids"}));
                    var refresh_date = new XDate();
                    localStorageService.add("menu", menu);
                    localStorageService.add("menu_refresh",refresh_date.toString("MMM d, yyyy"));  //new XDate());
                    //console.log(new XDate());
                    if (get_20_menu().length !=0) {
                        //$scope.500_link = _.filter(menu,function(m){ return m.name === "500"})[0].id;
                        $scope.l2_20_menu = get_level2_20_menu();
                        $scope.l3_20_menu = get_level3_20_menu();
                    }
                    if (get_30_menu().length !=0) {
                        //$scope.1000_link = _.filter(menu,function(m){ return m.name === "1000"})[0].id;
                        $scope.l2_30_menu = get_level2_30_menu();
                        $scope.l3_30_menu = get_level3_30_menu();
                    }
                    if (get_50_menu().length !=0) {
                        //$scope.1500_link = _.filter(menu,function(m){ return m.name === "1500"})[0].id;
                        $scope.l2_50_menu = get_level2_50_menu();
                        $scope.l3_50_menu = get_level3_50_menu();
                    }
                    if (get_75_menu().length !=0) {
                        //$scope.2000_link = _.filter(menu,function(m){ return m.name === "2000"})[0].id;
                        $scope.l2_75_menu = get_level2_75_menu();
                        $scope.l3_75_menu = get_level3_75_menu();
                    }
                    if (get_100_menu().length !=0) {
                        //$scope.2500_link = _.filter(menu,function(m){ return m.name === "2500"})[0].id;
                        $scope.l2_100_menu = get_level2_100_menu();
                        $scope.l3_100_menu = get_level3_100_menu();
                    }
                    if (get_150_menu().length !=0) {
                        //$scope.3000_link = _.filter(menu,function(m){ return m.name === "3000"})[0].id;
                        $scope.l2_150_menu = get_level2_150_menu();
                        $scope.l3_150_menu = get_level3_150_menu();
                    }
                    if (get_250_menu().length !=0) {
                        //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                        $scope.l2_250_menu = get_level2_250_menu();
                        $scope.l3_250_menu = get_level3_250_menu();
                    }
                     if (get_500_menu().length !=0) {
                        //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                        $scope.l2_500_menu = get_level2_500_menu();
                        $scope.l3_500_menu = get_level3_500_menu();
                    }
                    if (get_750_menu().length !=0) {
                        //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                        $scope.l2_750_menu = get_level2_750_menu();
                        $scope.l3_750_menu = get_level3_750_menu();
                    }
                     if (get_1000_menu().length !=0) {
                        //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                        $scope.l2_1000_menu = get_level2_1000_menu();
                        $scope.l3_1000_menu = get_level3_1000_menu();
                    }
                     if (get_1500_menu().length !=0) {
                        //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                        $scope.l2_1500_menu = get_level2_1500_menu();
                        $scope.l3_1500_menu = get_level3_1500_menu();
                    }
                     if (get_2000_menu().length !=0) {
                        //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                        $scope.l2_2000_menu = get_level2_2000_menu();
                        $scope.l3_2000_menu = get_level3_2000_menu();
                    }
                    if (get_3500_menu().length !=0) {
                        //$scope.3500_link = _.filter(menu,function(m){ return m.name === "3500"})[0].id;
                        $scope.l2_3500_menu = get_level2_3500_menu();
                        $scope.l3_3500_menu = get_level3_3500_menu();
                    }

                },
                function(){
                    //Display an error message
                    $scope.error= error;
                });
        }

    }
    function get_20_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "20"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "20"});
        }
        else{
                return [];
            }

    }
    function get_30_menu(){

        var tmp = _.filter(menu,function(m){ return m.name === "30"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "30"});
        }
        else{
            return [];
        }
    }
    function get_50_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "50"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "50"});
        }
        else{
            return [];
        }


    }
    function get_75_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "75"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "75"});
        }
        else{
            return [];
        }
    }

    function get_100_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "100"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "100"});
        }
        else{
            return [];
        }
    }

    function get_150_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "150"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "150"});
        }
        else{
            return [];
        }
    }
     function get_250_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "250"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "250"});
        }
        else{
            return [];
        }
    }
    function get_500_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "500"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "500"});
        }
        else{
            return [];
        }
    }
    function get_750_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "750"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "750"});
        }
        else{
            return [];
        }
    }
    function get_1000_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "1000"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "1000"});
        }
        else{
            return [];
        }
    }
     function get_1500_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "1500"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "1500"});
        }
        else{
            return [];
        }
    }
     function get_2000_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "2000"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "2000"});
        }
        else{
            return [];
        }
    }
     function get_3500_menu(){
        var tmp = _.filter(menu,function(m){ return m.name === "3500"});
        if (tmp.length > 0){
            return _.filter(menu,function(m){ return m.name === "3500"});
        }
        else{
            return [];
        }
    }


    function  get_level2_20_menu (){
        return _.where( get_20_menu()[0].menu_items,  {level:2});
    }
    function get_level3_20_menu (){
        return _.where( get_20_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_30_menu (){
        return _.where( get_30_menu()[0].menu_items,  {level:2});
    }
    function get_level3_30_menu (){
        return _.where( get_30_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_50_menu (){
        return _.where( get_50_menu()[0].menu_items,  {level:2});
    }
    function get_level3_50_menu (){
        return _.where( get_50_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_75_menu (){
        return _.where( get_75_menu()[0].menu_items,  {level:2});
    }
    function get_level3_75_menu (){
        return _.where( get_75_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_100_menu (){
        return _.where( get_100_menu()[0].menu_items,  {level:2});
    }
    function get_level3_100_menu (){
        return _.where( get_100_menu()[0].menu_items,  {level:3});
    }

    function  get_level2_150_menu (){
        console.log(get_150_menu()[0].menu_items);
        return _.where( get_150_menu()[0].menu_items,  {level:2});
    }
    function get_level3_150_menu (){
        return _.where( get_150_menu()[0].menu_items,  {level:3});
    }
     function  get_level2_250_menu (){
        return _.where( get_250_menu()[0].menu_items,  {level:2});
    }
    function get_level3_250_menu (){
        return _.where( get_250_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_500_menu (){
        return _.where( get_500_menu()[0].menu_items,  {level:2});
    }
    function get_level3_500_menu (){
        return _.where( get_500_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_750_menu (){
        return _.where( get_750_menu()[0].menu_items,  {level:2});
    }
    function get_level3_750_menu (){
        return _.where( get_750_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_1000_menu (){
        return _.where( get_1000_menu()[0].menu_items,  {level:2});
    }
    function get_level3_1000_menu (){
        return _.where( get_1000_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_1500_menu (){
        return _.where( get_1500_menu()[0].menu_items,  {level:2});
    }
    function get_level3_1500_menu (){
        return _.where( get_1500_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_2000_menu (){
        return _.where( get_2000_menu()[0].menu_items,  {level:2});
    }
    function get_level3_2000_menu (){
        return _.where( get_2000_menu()[0].menu_items,  {level:3});
    }
    function  get_level2_3500_menu (){
        return _.where( get_3500_menu()[0].menu_items,  {level:2});
    }
    function get_level3_3500_menu (){
        return _.where( get_3500_menu()[0].menu_items,  {level:3});
    }


});
