﻿/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 17/1/14
 * Time: 10:03 AM
 * To change this template use File | Settings | File Templates.
 */
app.controller('my_achievementController', function ($scope, $http, $q,
                                                 $location, my_achievementService,
                                                 localStorageService, utilService) {

    init();

    function init() {
        if (utilService.get_target_management() == 0) {
            $scope.target_management_indicator = false;
        }
        else {
            $scope.target_management_indicator = true;
            get_sales_achievement_data();
        }


    }

    function get_sales_achievement_data() {
        my_achievementService.get_sales_achievement_data($http, $q,
                        utilService.get_store(),
                        localStorageService.get('user_info').email_id)
                .then(function (data) {
                    $scope.achievements = data;
                    console.log($scope.achievements)
                },
            function () {
                //Display an error message
                $scope.error = error;
                $scope.loading = false;
            });
    }
    function get_emp_tree() {
        //        my_profileService.get_emp_tree($http, $q, localStorageService.get('user_info').email_id).then(function (data) {
        //                $scope.emp_tree = data;
        //            },
        //            function(){
        //                //Display an error message
        //                $scope.error= error;
        //                $scope.loading = false;
        //            });
    }

});