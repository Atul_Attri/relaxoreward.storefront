/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 14/9/13
 * Time: 1:22 PM
 * To change this template use File | Settings | File Templates.
 */
app.controller('MyorderController', function ($scope, $http, $q, $location, myorderService, localStorageService) {

    init();

    function init() {

        get_order_details();

    }

    function get_order_details(){
        var e = localStorageService.get('user_info');
        var u;
        if (e.hasOwnProperty('email_id')){
            u = e.email_id;
        }
        else {
            u= e.user_id;
        }
        myorderService.get_order_details($http, $q, u).then(function (data) {
                $scope.my_orders =  data;
                console.log(data);
            },
            function(){
                //Display an error message
                $scope.error= error;
                $scope.loading = false;
            });
    }

});
