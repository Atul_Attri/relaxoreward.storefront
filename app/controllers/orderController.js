﻿/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 27/7/13
 * Time: 4:19 PM
 * To change this template use File | Settings | File Templates.
 */

app.controller('orderController', function ($scope, $http, $q, orderService, utilService, cartService, checkoutService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below

    $scope.paymentInfo = {};

    $scope.paymentInfo = checkoutService.get_shipping_info();

    $scope.order = {};

    function init() {
       
        var tmp = orderService.get_Cart();
        if (tmp[0].id == null) {
            tmp.splice(0, 1);
        }
        $scope.cart = tmp;
        var total_amount_mrp, total_amount_final_offer;//, total_off;
        total_amount_mrp = 0;
        total_amount_final_offer = 0;
        //total_off = 0;
        //alert (tmp[0].final_offer * tmp[0].quantity);
        for (i = 0; i < tmp.length; i++) {
            total_amount_mrp = total_amount_mrp + tmp[i].mrp * tmp[i].quantity;
            total_amount_final_offer = total_amount_final_offer + tmp[i].final_offer * tmp[i].quantity;
        }
        $scope.total_amount_mrp = total_amount_mrp;
        $scope.total_amount_final_offer = total_amount_final_offer;
        $scope.cart.total_amt = total_amount_final_offer;
        $scope.total_discount = (((total_amount_mrp - total_amount_final_offer) / total_amount_mrp) * 100).toFixed(2);
        var conv_ratio = utilService.get_conversion_ratio();
        var store_type = utilService.get_store_type();
        if (store_type != "R") // store is not Retail Only
        {
            $scope.total_point = (total_amount_final_offer * conv_ratio).toFixed(2);

        }

        $scope.order.total_amount = total_amount_mrp;
        $scope.order.points = (total_amount_final_offer * conv_ratio).toFixed(2);
        $scope.order.discount_code = (((total_amount_mrp - total_amount_final_offer) / total_amount_mrp) * 100).toFixed(2);
        $scope.order.paid_amount = total_amount_final_offer;
       
        $scope.paymentInfo.Amount = total_amount_final_offer;
    }

    $scope.order.cart_id = cartService.get_cart_id();

    $scope.delete_from_cart = function (id) {

        orderService.delete_from_cart(id);
        $scope.cart = orderService.get_Cart();
    }
    $scope.update_cart = function (id, qty) {
        orderService.update_cart(id, qty);
    }
    init();

    $scope.Payment = function () {
        orderService.Payment($http, $q, $scope.order);
        $location.path("#/payment")
    }

});