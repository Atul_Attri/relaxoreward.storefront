/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 19/9/13
 * Time: 5:29 AM
 * To change this template use File | Settings | File Templates.
 */
app.controller('premiumController', function (
    $scope, $rootScope, $timeout,
    $location, $routeParams, $http,
    $q, premiumService, utilService,nextService,
    cartService, state_managerService, localStorageService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    var cat_id = $routeParams.catId;
    var premium_threshhold = utilService.get_premium_threshhold();
    init();
    $scope.sort_defn = "Sort By ..."
    $scope.$watch('sortorder', function () {
        switch ($scope.sortorder) {
            case "Name":
                $scope.sort_defn = "Name";
                break;
            case "+price.final_offer":
                $scope.sort_defn = "Price (Low to High)";
                break;
            case "-price.final_offer":
                $scope.sort_defn = "Price (High to Low)";
                break;
            case "-price.discount":
                $scope.sort_defn = "Overall Discount (high to low)";
                break;
            case "+brand":
                $scope.sort_defn = "Brand";
                break;
            default:
                $scope.sort_defn = "Sort By ...";
                break;
        }
    })

    function set_filter() {
        // get data
        //alert (cat_id);
        var cat_details, selected_cat;// = catService.get_cat_details(cat_id, $http, $q);
        premiumService.get_filters($http, $q, premium_threshhold).then(function (data) {
            //Update UI using data or use the data to call another service
            special_filters = data;
            $scope.filters = special_filters;
            //                console.log(special_filters );
        },
            function () {
                //Display an error message
                $scope.error = error;
            });
        $scope.checked_vals = ""

    }

    function check_login() {
        if (localStorageService.get('user_info') == null) {
            $location.path("/login");
        }
    }
    var brand_exclusion = utilService.get_brand_exclusion();
    function init() {
        check_login();
        //        $scope.cat_id = cat_id;
        $scope.sortorder = 'Name';
        set_filter();
        var prod_list;
        user_info = localStorageService.get('user_info');
        // engineering debt: we are using similar DB call for this and get_filter function
        // can be optimized
        premiumService.get_premium_prod_list($http, $q, premium_threshhold, user_info.min_point_range, user_info.max_point_range).then(function (data) {
            //Update UI using data or use the data to call another service
            prod_list = data;
            process_price_logic(prod_list);

            get_brands();
            //console.log(prod_list)  ;
        },
            function () {
                //Display an error message
                $scope.error = error;
            });
        // search brands within searched prod list

    }
    var process_price_logic = function (prod_list) {
        var productList = [];
        var conv_ratio = utilService.get_conversion_ratio();
        var store_type = utilService.get_store_type();
        var customer_discount = utilService.get_customer_discount();
        var cat_discount = utilService.get_cat_discount();
        var sku_discount = utilService.get_sku_discount();

        var brand_discount = utilService.get_brand_discount();

        var arr_cat, arr_sku, special_price;
        special_price = 0;
        $scope.store_type = store_type;

        var filter_display = utilService.get_filter_display(); //Added By Hassan Ahamed 
        $scope.filter_display = filter_display; //Added By Hassan Ahamed Start

        //if (store_type !=  "R") // store is not Retail Only
        //{
        for (i = 0; i < prod_list.length; i++) {
            //console.log(prod_list[i]) ;
            prod = JSON.parse(prod_list[i].toString());
            if ((typeof prod.price) == "object") {
                prod_price = prod.price;
            }
            else {
                prod_price = JSON.parse(prod.price.toString());
            }

            if (customer_discount != "") {
                prod_price.final_offer = prod_price.final_offer * (1 - parseFloat(customer_discount).toFixed(2));
                special_price = 1;
                //console.log(prod.price.final_offer);
            }
            arr_cat = prod.cat_id; // cats this product belongs to
            //for (k = 0; k < cat_discount.length; k++) // for each of deal cats
            //{
            //    if (arr_cat.indexOf(cat_discount[k].id) > -1) {
            //        prod_price.final_offer = prod_price.final_offer * (1 - parseFloat(cat_discount[k].extra_off).toFixed(2));
            //        special_price = 1;
            //    }
            //}
            //for (k = 0; k < sku_discount.length; k++) // for each of deal cats
            //{
            //    if (prod.sku == sku_discount[k].id) {
            //        prod_price.final_offer = prod.price_final_offer * (1 - parseFloat(sku_discount[k].extra_off).toFixed(0));
            //        special_price = 1;
            //    }
            //}

            for (k = 0; k < cat_discount.length; k++) // for each of deal cats
            {
                if (arr_cat.indexOf(cat_discount[k].id) > -1) {
                    prod_price.final_offer = prod_price.final_offer * (1 - cat_discount[k].extra_off);
                    special_price = 1;
                }
            }
            /* for (k = 0; k < sku_discount.length; k++) // for each of deal cats
            {
                if (prod.sku == sku_discount[k].id) {
                    prod_price.final_offer = prod.price_final_offer * (1 - parseFloat(sku_discount[k].extra_off).toFixed(3));
                    special_price = 1;
                }
            }*/
            for (k = 0; k < sku_discount.length; k++) // for each of deal cats
            {
                if (prod.sku == sku_discount[k].id) {
                    prod_price.final_offer = prod_price.final_offer * (1 - sku_discount[k].extra_off);
                    special_price = 1;
                }
            }


            prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));


            /**************Added By To Calculate Special Discount For Brand***********/
            for (k = 0; k < brand_discount.length; k++) // for each of deal cats
            {
                if (prod.brand == brand_discount[k].brand) {
                    prod_price.final_offer = prod_price.final_offer * (1 - brand_discount[k].extra_off);
                    special_price = 1;
                }
            }

            prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));

            /**********************************************************/



            prod_price.final_offer = parseInt(prod_price.final_offer.toFixed(0));
            prod_price.final_discount = (1 - (prod_price.final_offer / prod_price.mrp)) * 100;
            // prod_price.final_discount = parseInt(prod_price.final_discount.toFixed(0));
            prod_price.final_discount = prod_price.final_discount.toFixed(2);//Diwakar
            prod.point = prod_price.final_offer * conv_ratio;
            prod.point = prod.point.toFixed(0);





            prod.price = prod_price;
            //productList.push(prod);
            var excluded = false;
            //console.log(prod.brand.toLowerCase())   ;
            if ((prod.stock < 0) && (prod.stock != -9999)) {
                excluded = true;
            }
            if (brand_exclusion.length > 0) {
                for (j = 0; j < brand_exclusion.length; j++) {
                    if (prod.brand.toLowerCase() === brand_exclusion[j].toLowerCase()) {
                        console.log(prod.brand.toLowerCase());
                        excluded = true;
                        break;
                    }
                }
            }
            if (excluded === false) {
                productList.push(prod);
            }
        }
        //}
        //alert (productList[0].price.list);

        nextService.nextList = productList;

        if (filter_display == "P") {//Added By Hassan Ahamed Start

            if (productList.length > 0) {
                $scope.productList = productList;
                $scope.productList_original = productList;
                $scope.special_price = special_price;
                //console.log(productList);
                $scope.max_price_sku = _.max($scope.productList_original,
                    function (p) {
                        return parseInt(p.point);
                    }).point;
                $scope.min_price_sku = _.min($scope.productList_original,
                        function (p) {
                            return parseInt(p.point);
                        }).point;
            }
        }

        else {//Added By Hassan Ahamed End

            if (productList.length > 0) {
                $scope.productList = productList;
                $scope.productList_original = productList;
                $scope.special_price = special_price;
                console.log(productList);

                $scope.max_price_sku = _.max($scope.productList_original,
                        function (p) {
                            return parseInt(p.price.final_offer);
                        }).price.final_offer;
                $scope.min_price_sku =
                _.min($scope.productList_original,
                        function (p) {
                            return parseInt(p.price.final_offer);
                        }).price.final_offer;
            }
        }
    }
    var get_brands = function () {
        var brands_filter, tmp = [];
        for (i = 0; i < $scope.productList.length; i++) {
            var excluded = false;
            var prod_brand = $scope.productList[i].brand;
            console.log(prod_brand.toLowerCase());
            for (j = 0; j < brand_exclusion.length; j++) {
                if (prod_brand.toLowerCase() === brand_exclusion[j].toLowerCase()) {
                    excluded = true;
                    break;
                }
            }
            if (excluded === false) {
                tmp.push(
                    prod_brand.toUpperCase()
                )
            }

        }
        $scope.brand_filter = _.uniq(tmp);
    }

    var filters = [];
    $scope.apply_filter = function (filter_name, filter_value) {

        var found = false;
        for (i = 0; i < filters.length; i++) {
            if (filters[i].name == filter_name) {
                if (filters[i].value == filter_value) {
                    if ((filter_name != "discount") && (filter_name != "price")) {
                        filters.splice(i, 1);
                        found = true;
                    }
                }
            }
        }

        if (!found) {
            filters.push({ name: filter_name, value: filter_value });
        }
        if (filter_name == "price") {
            var filter_price = true;
        }
        if (filter_name == "discount") {
            var filter_discount = true;
        }
        var brand_filter = _.where(filters, { name: "brand" });
        var price_filter = _.where(filters, { name: "price" });
        var discount_filter = _.where(filters, { name: "discount" });
        var feature_filter = _.difference(filters, brand_filter, price_filter, discount_filter);// .without(filters, {name:"brand"});

        //        console.log(feature_filter );

        // apply filter on original productlist and copy to display one
        var prodlist_tmp = $scope.productList_original;
        if (filters.length != 0) {
            var l_found;
            var l_brand_found;

            var prodList_filtered = [];
            var prod_list_feature_filtered = [];
            var prod_list_price_filtered = [];
            var prod_list_discount_filtered = [];

            // first filter for features

            if (feature_filter.length > 0) {
                for (i = 0; i < prodlist_tmp.length; i++) {
                    var prod = prodlist_tmp[i];
                    var prod_features = prodlist_tmp[i].feature;
                    l_found = false;
                    if (feature_filter.length > 0) {
                        for (j = 0; j < feature_filter.length ; j++) {
                            for (k = 0; k < prod_features.length; k++) {
                                if ((prod_features[k].name != null)
                                    &&
                                    (feature_filter[j].name != null)
                                    &&
                                    (prod_features[k].values != null)
                                    &&
                                    (feature_filter[j].value != null)) {
                                    if (prod_features[k].name.toLowerCase() == feature_filter[j].name.toLowerCase()) {
                                        //l_found = false;
                                        if (prod_features[k].values.toLowerCase() == feature_filter[j].value.toLowerCase()) {
                                            l_found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if (l_found) {
                            prod_list_feature_filtered.push(prod);
                        }
                    }
                    else {
                        prod_list_feature_filtered = prodlist_tmp;
                    }
                }
            }
            else {
                prod_list_feature_filtered = prodlist_tmp;
            }
            // now filter for brand
            if (brand_filter.length > 0) {
                for (i = 0; i < prod_list_feature_filtered.length; i++) {
                    l_brand_found = false;
                    var prod_tmp = prod_list_feature_filtered[i];
                    for (p = 0; p < brand_filter.length ; p++) {
                        if (prod_tmp.brand.toLowerCase() == brand_filter[p].value.toLowerCase()) {
                            l_brand_found = true;
                            break;
                        }
                    }
                    if (l_brand_found) {
                        prodList_filtered.push(prod_tmp);
                    }

                }
            }
            else {
                prodList_filtered = prod_list_feature_filtered;
            }
            //console.log(prodList_filtered);
            // now filter for pricerange apply that on prodList_filtered
            if (filter_price) {

                for (i = 0; i < prodList_filtered.length; i++) {
                    var prod_tmp = prodList_filtered[i];

                    if (filter_display == "P") { //Added By Hassan Ahamed Start

                        if (
                            (parseInt(prod_tmp.point) >= $scope.min_price_sku)
                            &&
                            (parseInt(prod_tmp.point) <= $scope.max_price_sku)
                            ) {
                            // push to new prodlist
                            prod_list_price_filtered.push(prod_tmp);
                        }

                    }
                    else {//Added By Hassan Ahamed End

                        //console.log(prod_tmp);
                        if (
                            (parseInt(prod_tmp.price.final_offer) >= $scope.min_price_sku)
                                &&
                                (parseInt(prod_tmp.price.final_offer) <= $scope.max_price_sku)
                            ) {
                            // push to new prodlist
                            prod_list_price_filtered.push(prod_tmp);
                        }
                    }
                }
                prodList_filtered = prod_list_price_filtered;
            }

            if (filter_discount) {


                for (i = 0; i < prodList_filtered.length; i++) {
                    var prod_tmp = prodList_filtered[i];
                    //console.log(prod_tmp);

                    switch (parseInt($scope.discount_filter)) {
                        case 10:
                            // 0 - 10
                            console.log("0-10");
                            if ((parseInt(prod_tmp.price.final_discount) >= 0)
                                &&
                                (parseInt(prod_tmp.price.final_discount) <= 10)) {
                                prod_list_discount_filtered.push(prod_tmp);
                            }

                            break;
                        case 25:
                            // 10-25
                            console.log("10-25");
                            if ((parseInt(prod_tmp.price.final_discount) >= 10)
                                &&
                                (parseInt(prod_tmp.price.final_discount) <= 25)) {
                                prod_list_discount_filtered.push(prod_tmp);
                            }
                            break;
                        case 100:
                            // >25
                            if (parseInt(prod_tmp.price.final_discount) >= 25) {
                                prod_list_discount_filtered.push(prod_tmp);
                            }
                            break;
                        default:
                            console.log("default");
                            prod_list_discount_filtered.push(prod_tmp);
                            break;
                    }
                }
                prodList_filtered = prod_list_discount_filtered
            }

            // *********************
            $scope.productList = prodList_filtered;

        }
        else {
            $scope.productList = $scope.productList_original;
        }


    }

    $scope.addTocart = function (productDetails) {



        if (productDetails.have_child == 1) {
            state_managerService.refer_url = $location.path();
            var url = "/quickbuy/" + productDetails.id + "/1";
            $location.path(url);
        }
        else {
            $timeout(function () {
                cartService.addTocart(productDetails);
                //$scope.cart_size = cartService.get_Cart_Size();
                $rootScope.$broadcast('cart_changed');
            }, 500);
        }
    }

    $scope.openQuickBuy = function (product_id) {
        state_managerService.refer_url = $location.path();
        $location.path('/quickbuy/' + product_id);
    }


});
