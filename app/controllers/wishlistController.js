/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 27/7/13
 * Time: 4:19 PM
 * To change this template use File | Settings | File Templates.
 */

app.controller('wishlistController', function ($scope, $window, $location,
                                           $timeout, $rootScope, $http, $q, loginService,
                                            wishlistService, utilService, localStorageService , acctService,cartService) {

    //I like to have an init() for controllers that need to perform some initialization.


   // var point = 100;
   // $scope.user_point = parseInt(acctService.get_points()).toFixed(0);
    var user_points = 0;
    user_points = parseInt(acctService.get_points()).toFixed(0);
   // user_points = localStorageService.get('user_points');
    $scope.percentage = {};
    $scope.remain = {};
    $scope.progress  = user_points;
    var point = $scope.progress;
    $scope.max_point = "";
    $scope.wishlist = {};

    $scope.$on('wishlist_changed', function () {
        // This function listens to any change in scope for broadcast message wishlist_changed.
        // If it gets that, applies subsequent code
        //console.log(localStorageService.get('local_wishlist'));

       // alert('Wishlist Changed');

        var tmp = wishlistService.get_wishlist();
      //  alert(tmp);
        console.log(tmp);
        if (tmp.length > 0) {
            for (i = 0; i < tmp.length; i++) {
                console.log(tmp[i].id + "-" + tmp[i].quantity);
                if (tmp[i].quantity == 0) {
                    console.log("to be deleted");
                    $scope.delete_from_wishlist(tmp[i].id);
                }
            }
        }
		else{
		
		   $scope.wishlist_size = 0;
		
		   }
        //console.log(tmp);
        apply_calculation(tmp);

        wishlistService.update_wishlist();

        if ((localStorageService.get('local_wishlist') != null)
            &&
            (localStorageService.get('local_wishlist') != "")
            ) {

            wishlistService.save_wishlist_to_db($http, $q).then(function (data) {
                //alert("Cart is updated successfully");
            },
                function () {
                    //Display an error message
                    $scope.error = error;
                });
        }

    });
    $scope.$on('wishlist_loaded', function () {

    //    alert('Hello');

        // This function listens to any change in scope for broadcast message wishlist_changed.
        // If it gets that, applies subsequent code
        //console.log(localStorageService.get('local_wishlist'));
        var conv_ratio = utilService.get_conversion_ratio();
        var store_type = utilService.get_store_type();
        $scope.store_type = store_type;
        $scope.conv_ratio = conv_ratio;
        //console.log($scope.store_type) ;
        var tmp = wishlistService.get_wishlist();
        //console.log(tmp);
        apply_calculation(tmp);
        //console.log("entering update_cart");
        wishlistService.update_wishlist();
    });
    function init() {
        // check user as this is loaded in all pages
        check_login();

        //Added By Hassan Ahamed Start

        var Refresh_Time_Duration = 0;
        var Login_Dt_Time = "";
        var Current_Dt_Time = "";

        if (localStorageService.get('login_date_time') != null) {
            Login_Dt_Time = localStorageService.get('login_date_time');
            var myLoginDtTime = new Date(Login_Dt_Time);
            Current_Dt_Time = new Date();

            var Login_Date = myLoginDtTime.getDate();
            var Current_Date = Current_Dt_Time.getDate();
            if (Login_Date != Current_Date) {
                Refresh_Time_Duration = 13;
            }
            else {
                var date1_ms = myLoginDtTime.getTime();
                var date2_ms = Current_Dt_Time.getTime();
                var difference_ms = date2_ms - date1_ms;
                //take out milliseconds
                difference_ms = difference_ms / 1000;
                var seconds = Math.floor(difference_ms % 60);
                difference_ms = difference_ms / 60;
                var minutes = Math.floor(difference_ms % 60);
                difference_ms = difference_ms / 60;
                Refresh_Time_Duration = Math.floor(difference_ms % 24);
            }
        }

        if (Refresh_Time_Duration > 12) {

            loginService.get_wishlist_by_email($http, $q, localStorageService.get('user_info').email_id).then(function (data) {
                //Update UI using data or use the data to call another service
                wishlistService.cart = data;
                localStorageService.add('local_wishlist', data);
                $rootScope.$broadcast('wishlist_loaded');
            },
            function () {
                //Display an error message
                $scope.error = error;
            });

        }

        //Added By Hassan Ahamed End

        var tmp = wishlistService.get_wishlist();
        $scope.wishlist = tmp;
        if (localStorageService.get('express_shipping') != null) {
            $scope.express_shipping = localStorageService.get('express_shipping');
        }
        else {
            $scope.express_shipping = 0;
        }
        apply_calculation(tmp);
        if (localStorageService.get('user_info') != null) {
            $scope.user_points = localStorageService.get('user_info').total_point;
        }
    }
	
	 function get_wishlist_data()
	 {
	     var tmp = wishlistService.get_wishlist();
        $scope.wishlist = tmp;
		
		
	 
	 }
	
    function check_login() {
        if (localStorageService.get('user_info') == null) {
            //$window.location.href = root_url + "/index.html#/login";
            $window.location.href = root_url + "/login/login.html#/login";
        }
    }

    var cal = function()
    {
        $scope.percentage= Math.round((point *100 / $scope.max_point));
        $scope.remain = Math.round(100 -  $scope.percentage );
    }


    var apply_calculation = function (tmp) {
        var conv_ratio = utilService.get_conversion_ratio();
        var store_type = utilService.get_store_type();
        $scope.store_type = store_type;
        $scope.conv_ratio = conv_ratio;
        //console.log($scope.store_type) ;

        if (tmp.length > 0) {
            if (tmp[0].id == null) {
                tmp.splice(0, 1);
            }

            var total_amount_mrp, total_amount_final_offer;//, total_off;
            total_amount_mrp = 0;
            total_amount_final_offer = 0;
            for (var i = 0; i < tmp.length; i++) {

                total_amount_mrp = total_amount_mrp + tmp[i].mrp * tmp[i].quantity;
                total_amount_final_offer = total_amount_final_offer + tmp[i].final_offer * tmp[i].quantity;
                tmp[i].discount = ((1 - (total_amount_final_offer / total_amount_mrp)) * 100).toFixed(2);//Diwakar

                $scope.max_point = (tmp[i].final_offer * tmp[i].quantity);
                $scope.prog_width = point * 100 / ( $scope.max_point ) + "%";
                $scope.percentage= Math.round((point *100 / $scope.max_point));
                $scope.remain = Math.round(100 -  $scope.percentage );
                $scope.remain_points = ($scope.max_point - point) * 1 ;
                if($scope.percentage >= 100 )
                {
                    $scope.percentage = 100;
                    $scope.remain = 0;
                }
                else if($scope.remain_points < 0)
                {
                    $scope.remain_points = 0;

                }

//                $scope.progress_bar = {
//
//                    percentage:$scope.percentage,
//                    percentage_remain:$scope.remain
//
//                };
                tmp[i].percentage = $scope.percentage;
                tmp[i].percentage_remain =  $scope.remain;
                tmp[i].point_remain = $scope.remain_points;
				if(tmp[i].point_remain < 0)
				{
				tmp[i].target_point = 0;
				    
				}
				else
				{
				 tmp[i].target_point = tmp[i].point_remain;
				  
				}

               // tmp[i].push($scope.progress_bar);
               // cal();
                //console.log (tmp[i].discount);
                // tmp[i].discount =(tmp[i].discount*1).toFixed(2); //Diwakar
            }
            $scope.wishlist = tmp;
          //  $scope.wishlist.push($scope.progress_bar);

            $scope.wishlist_length = tmp.length;
			$scope.wishlist_size = $scope.wishlist_length;
           // $scope.wishlist_size = wishlistService.get_wishlist_Size();

            //console.log(tmp);
            $scope.total_amount_mrp = total_amount_mrp;
            $scope.total_amount_final_offer = total_amount_final_offer;
            var min_shipping = utilService.get_min_shipping();
            var shipping_charge = utilService.get_shipping_charge();
            var shipping = 0;
            if (total_amount_final_offer < min_shipping) {
                shipping = $scope.shipping = shipping_charge;
            }
            else {
                shipping = $scope.shipping = 0;
            }
            $scope.total_amount_billable = total_amount_final_offer + shipping + parseInt($scope.express_shipping);

            $scope.total_discount = (((total_amount_mrp - total_amount_final_offer) / total_amount_mrp) * 100).toFixed(0);

            if (store_type != "R") // store is not Retail Only
            {
                $scope.total_point = (total_amount_final_offer * conv_ratio).toFixed(2);

            }
        }
    }


    $scope.triggerCheckOut = function () {
        //        console.log($location);
        //        console.log($window.location.uri);
        //$window.location = "index.html#/checkout";
        //$location.path("/checkout");
        $window.location.href = root_url + "/index.html#/checkout"
    }
    $scope.delete_from_wishlist = function (id) {
        console.log(id)

        var val = confirm('Do You want to delete a product from Wishlist?');

        if(val == true) {

            $timeout(function () {

            wishlistService.delete_from_wishlist(id);
            $rootScope.$broadcast('wishlist_changed');

            }, 1000);


			// get_wishlist_data();
			 
			
			// $scope.wishlist_length = tmp.length;
			// $scope.wishlist_size = $scope.wishlist_length;

        }

    }
	
	$scope.move_from_wishlist = function (id) {

        console.log(id)

        var val = confirm('Do You want to move a product from Wishlist?');

        if(val == true) {

            $timeout(function () {

            wishlistService.delete_from_wishlist(id);



            $rootScope.$broadcast('wishlist_changed');

            }, 1000);
			
			//get_wishlist_data();
			
			//$scope.wishlist_length = tmp.length;
			
			//$scope.wishlist_size = $scope.wishlist_length;



        }

    }

    $scope.addTocart = function (productDetails) {
        $scope.show_loader=true;
      //  var product_name = productDetails.name;
      //  var product_image = productDetails.image.thumb_link;
        //cartService.addTocart(productDetails.id, 1, productDetails.price.mrp, productDetails.price.mrp);


        if (($scope.myOptions != "") && ($scope.selected_prod == "")) {
            // this is a condition when we have sizes but it's not selected
            alert("Please select size !");
        }
        else {
            //    $scope.show_loader=true;
            $timeout(function () {
			
			    $scope.move_from_wishlist(productDetails.id); 
                cartService.addTocart_from_wishlist(productDetails, productDetails.selected_prod, productDetails.myOptions,productDetails.image);

               // $scope.prod_image = product_image;
                $scope.show_loader=false;
              
				alert('Item Has Been Moved To Cart');

                //$scope.cart_size = cartService.get_Cart_Size();
                $rootScope.$broadcast('cart_changed');
            }, 1000);
			
		

		
	

      
			
		
        }
    }


    $scope.saveCart = function () {
        //console.log(cart);
        //wishlistService.update_cart(id, qty);
    }

    $scope.update_cart = function (id, qty) {
        wishlistService.update_cart(id, qty);
    }
    init();

    //$scope.add_cart = function () {
    //    wishlistService.add_cart($http, $q, $scope.cart);
    //}
    //$scope.update_cart_item_size = function () {

    //    for (i = 0; i < $scope.cart.length; i++) {
    //        for (j = 0; j < $scope.cart[i].sizes.length; j++) {
    //            if ($scope.cart[i].selected_size === $scope.cart[i].sizes[j].size) {
    //                $scope.cart[i].sku = $scope.cart[i].sizes[j].sku;
    //                $scope.cart[i].cart_item_id = $scope.cart[i].sizes[j].sku;

    //            }
    //        }
    //        //if ($scope.cart[i])
    //    }
    //    wishlistService.update_to_new_cart($scope.cart);

    //    $rootScope.$broadcast('wishlist_changed');
    //    //alert ($scope.cart);
    //    //wishlistService.update_cart_item_size();
    //}
    $scope.$watch('$scope.cart.sku', function () {
        $rootScope.$broadcast('wishlist_changed');
    })

});