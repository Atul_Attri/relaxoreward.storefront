/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 31/7/13
 * Time: 11:03 PM
 * To change this template use File | Settings | File Templates.
 */

app.directive('buttonsRadio', function () {
    return {
        restrict: 'E',
        scope: { model: '=', options:'=', sku_sizes:"="},
        controller: function($scope){
            $scope.activate = function(option){
                $scope.model = option;
            };
        },
        template: "<button type='button' class='btn' "+
            "ng-class='{active: option == model}'"+
            "ng-repeat='option in options' "+
                "ng-click='activate(option)'>{{option.size}} "+
            "</button>"
    }
});