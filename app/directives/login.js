/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 22/8/13
 * Time: 5:46 AM
 * To change this template use File | Settings | File Templates.
 */
app.directive('loginLink', function () {

// I allow an instance of the directive to be hooked
    // into the user-interaction model outside of the
    // AngularJS context.
    function link( $scope, element, attributes ) {

        // I am the TRUTHY expression to watch.
        var expression = attributes.isVisible;
        // I am the optional slide duration.
        var duration = ( attributes.slideShowDuration || "fast" );


        // I check to see the default display of the
        // element based on the link-time value of the
        // model we are watching.
        if ( ! $scope.$eval(expression ) ) {

            element.hide();

        }


        // I watch the expression in $scope context to
        // see when it changes - and adjust the visibility
        // of the element accordingly.
        $scope.$watch(
            expression,
            function( ) {
                if ( ! $scope.$eval( expression ) ) {

                    element.hide();

                }
                else
                {
                element.show();
                }


            }
        );

    }

    return({
        link: link,
        restrict: "A"
    });
});