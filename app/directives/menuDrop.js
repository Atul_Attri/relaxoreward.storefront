/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 22/8/13
 * Time: 8:21 PM
 * To change this template use File | Settings | File Templates.
 */
app.directive('showonhoverparent', function () {
            return {
                link : function(scope, element, attrs) {
                    element.parent().bind('mouseenter', function() {
                        element.show();
                    });
                    element.parent().bind('mouseleave', function() {
                        element.hide();
                    });
                }
            };
});