/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 14/7/13
 * Time: 7:20 AM
 * To change this template use File | Settings | File Templates.
 */


app.service('adService', function () {
    this.get_ads = function () {

        return  adItems.landing_page_main_scroller;
    };
    this.get_home_banner = function($http, $q){
        //var store = utilService.get_store();
        var apiPath = cat_service_url + '/ad/home/banner?json=true' ;
        //alert (apiPath);
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }
    this.get_home_static_html = function($http, $q){
        //var store = utilService.get_store();
        var apiPath = cat_service_url + '/ad/home/static?json=true' ;
        //alert (apiPath);
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }
    this.get_level1_banner = function($http, $q){
        //var store = utilService.get_store();
        var apiPath = cat_service_url + '/ad/level1/banner?json=true' ;
        //alert (apiPath);
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }

    this.get_cat_level1_ads = function (cat_id) {
        var cat_ads = adItems.cat_level1_scroller;
        var selected_cat_ads = [];
        for (i=0; i< cat_ads.length; i++){
            if (cat_id == cat_ads[i].id) {
                selected_cat_ads.push(cat_ads[i]);
            }
        }
        return selected_cat_ads;

    };


});

