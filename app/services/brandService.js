/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 29/7/13
 * Time: 7:08 PM
 * To change this template use File | Settings | File Templates.
 */
app.service('brandService', function () {


    this.get_product_by_brand = function ($http, $q, brand_name) {
        //var apiPath = 'http://localhost:8181/store/prod_list_by_cat_short?cat_id=' + cat_id + '&json=true';
        //console.log(max) ;
        var apiPath = cat_service_url + '/store/get_prod_by_brand?brand_name=' + brand_name + '&json=true';
        //console.log(apiPath);
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            //data: data,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("An error occured while validating User");
        })

        return deferred.promise;
    };


    //this.get_product_list_by_brand = function (brand_name) {
    //    var selected_product_list = [];

    //    for (i=0; i< productList.length; i++) {
    //        if (productList[i].brand == brand_name){
    //            selected_product_list.push(productList[i]);
    //        }
    //    }
    //    return  selected_product_list;
    //};

    //this.get_product_list_by_brand = function ($http, $q, brand){
    //    var apiPath = cat_service_url + "/store/prod_list_by_brand/" + brand + "?json=true";
    //    var deferred = $q.defer();
    //    $http({
    //        method: 'GET',
    //        url: apiPath,
    //        //data: data,
    //        type: JSON
    //    }).success(function (data) {
    //            deferred.resolve(data);
    //        }).error(function (data) {
    //            deferred.reject("An error occured while validating User");
    //        })

    //    return deferred.promise;
    //};

    //this.get_brand_details = function ($http, $q, brand_id, min, max) {
    //    //var apiPath = 'http://localhost:8181/store/prod_list_by_cat_short?cat_id=' + cat_id + '&json=true';
    //    //console.log(max) ;
    //    var apiPath = cat_service_url + 'store/brand_details/'+brand_id+ '?json=true';
    //    //console.log(apiPath);
    //    var deferred = $q.defer();
    //    $http({
    //        method: 'GET',
    //        url: apiPath,
    //        //data: data,
    //        type: JSON
    //    }).success(function (data) {
    //        deferred.resolve(data);
    //    }).error(function (data) {
    //        deferred.reject("An error occured while validating User");
    //    })

    //    return deferred.promise;
    //};

    //this.get_categories_by_brand = function ($http, $q, brand){
    //    var apiPath = cat_service_url + "/store/categories_by_brand/" + brand + "?json=true";
    //    var deferred = $q.defer();
    //    $http({
    //        method: 'GET',
    //        url: apiPath,
    //        //data: data,
    //        type: JSON
    //    }).success(function (data) {
    //            deferred.resolve(data);
    //        }).error(function (data) {
    //            deferred.reject("An error occured while validating User");
    //        })

    //    return deferred.promise;
    //};



    //this.test = function ($http, $q){
    //    var apiPath='http://localhost:7898/store/prod_list_by_cat/51bb3352782b4c22745a50fc?json=true';
    //    var deferred = $q.defer();
    //    $http.get(apiPath)
    //        .success(function (data) {
    //            deferred.resolve(data);
    //        })
    //        .error(function () {
    //            deferred.reject("An error occured while fetching items");
    //        });
    //    return deferred.promise;
    //};

    //    var productList = [
    //        {
    //            "id": "1",
    //            "cat_id": ["1.1","2.1"],
    //            "sku":"1234",
    //            "brand":"nike",
    //            "price" :
    //            {
    //                "mrp" : "2100",
    //                "list" : "1400",
    //                "discount" : "30",
    //                "min" : "1000",
    //                "final_offer":"1400",
    //                "final_discount": "30"
    //            },
    //            "point":"",
    //            Name: '501 Levis Jeans - 42',
    //            description: 'Levis 5 pocket regular jeans',
    //            image:'https://s3-ap-southeast-1.amazonaws.com/annectos/test.jpg '
    //        },
    //        {
    //            id: '2',
    //            cat_id: "1.2",
    //            "sku":"1234-1",
    //            "brand":"nike",
    //            "price" :
    //            {
    //                "mrp" : "1500",
    //                "list" : "750",
    //                "discount" : "50",
    //                "min" : "500",
    //                "final_offer":"750" ,
    //                "final_discount": "50"
    //            },
    //            "point":"",
    //            Name: '501 Levis Jeans - 40', description: 'Levis 5 pocket regular jeans' , image:'https://s3-ap-southeast-1.amazonaws.com/annectos/test.jpg '
    //        },
    //        {
    //            id: '3',
    //            cat_id: "1.1",
    //            "sku":"1234-2",
    //            "brand":"puma",
    //            "price" :
    //            {
    //                "mrp" : "2100",
    //                "list" : "1400",
    //                "discount" : "30",
    //                "min" : "1000",
    //                "final_offer":"1400",
    //                "final_discount": "30"
    //            },
    //            "point":"",
    //            Name: '501 Levis Jeans - 38', description: 'Levis 5 pocket regular jeans'  , image:'https://s3-ap-southeast-1.amazonaws.com/annectos/test.jpg '
    //        },
    //        {
    //            id: '4',
    //            cat_id: "1.1",
    //            "sku":"1234-3",
    //            "brand":"polo",
    //            "price" :
    //            {
    //                "mrp" : "1500",
    //                "list" : "750",
    //                "discount" : "50",
    //                "min" : "500",
    //                "final_offer":"750",
    //                "final_discount": "50"
    //            },
    //            "point":"",
    //            Name: '501 Levis Jeans - 36', description: 'Levis 5 pocket regular jeans'  , image:'https://s3-ap-southeast-1.amazonaws.com/annectos/test.jpg '
    //        },
    //        {
    //            id: '5',
    //            cat_id: "2.1",
    //            "sku":"1234-4",
    //            "brand":"dockers",
    //            "price" :
    //            {
    //                "mrp" : "1500",
    //                "list" : "750",
    //                "discount" : "50",
    //                "min" : "500",
    //                "final_offer":"750",
    //                "final_discount": "50"
    //            },
    //            "point":"",
    //            Name: '501 Levis Jeans - 34', description: 'Levis 5 pocket regular jeans'  , image:'https://s3-ap-southeast-1.amazonaws.com/annectos/test.jpg '
    //        },
    //        {
    //            id: '6',
    //            cat_id: "1.3",
    //            "sku":"1234-5",
    //            "brand":"addidas",
    //            "price" :
    //            {
    //                "mrp" : "2100",
    //                "list" : "1400",
    //                "discount" : "30",
    //                "min" : "1000",
    //                "final_offer":"1400",
    //                "final_discount": "30"
    //            },
    //            "point":"",
    //            Name: '501 Levis Jeans - 34', description: 'Levis 5 pocket regular jeans' , image:'https://s3-ap-southeast-1.amazonaws.com/annectos/test.jpg '
    //        },
    //        {
    //            id: '7',
    //            cat_id: "1.2",
    //            "sku":"7890",
    //            "brand":"hagger",
    //            "price" :
    //            {
    //                "mrp" : "2100",
    //                "list" : "1400",
    //                "discount" : "30",
    //                "min" : "1000",
    //                "final_offer":"1400",
    //                "final_discount": "30"
    //            },
    //            "point":"",
    //            Name: '501 Levis Jeans - 34', description: 'Levis 5 pocket regular jeans' , image:'https://s3-ap-southeast-1.amazonaws.com/annectos/test.jpg '
    //        },
    //        {
    //            id: '8', cat_id: "1.1",
    //            "sku":"7890-1",
    //            "brand":"nike",
    //            "price" :
    //            {
    //                "mrp" : "2100",
    //                "list" : "1400",
    //                "discount" : "30",
    //                "min" : "1000",
    //                "final_offer":"1400",
    //                "final_discount": "30"
    //            },
    //            "point":"",
    //            Name: '501 Levis Jeans - 34', description: 'Levis 5 pocket regular jeans'    , image:'https://s3-ap-southeast-1.amazonaws.com/annectos/test.jpg '
    //        },
    //        {
    //            id: '9', cat_id: "4.1",
    //            "sku":"7890-2",
    //            "brand":"local",
    //            "price" :
    //            {
    //                "mrp" : "2100",
    //                "list" : "1400",
    //                "discount" : "30",
    //                "min" : "1000",
    //                "final_offer":"1400" ,
    //                "final_discount": "30"
    //            },
    //            "point":"",
    //            Name: '501 Levis Jeans - 34', description: 'Levis 5 pocket regular jeans' , image:'https://s3-ap-southeast-1.amazonaws.com/annectos/test.jpg '
    //        },
    //        {
    //            id: '10', cat_id: "1.1",
    //            "sku":"7890-3",
    //            "brand":"all",
    //            "price" :
    //            {
    //                "mrp" : "1500",
    //                "list" : "750",
    //                "discount" : "50",
    //                "min" : "500",
    //                "final_offer":"750",
    //                "final_discount": "50"
    //            },
    //            "point":"",
    //            Name: '501 Levis Jeans - 34', description: 'Levis 5 pocket regular jeans' , image:'https://s3-ap-southeast-1.amazonaws.com/annectos/test.jpg '
    //        }
    //
    //
    //    ];

    //this.get_prod_by_cat_filter = function ($http, $q, brand_name, unsorted_filter) {
    //    var qs = "{brand_name:{brand_name:'" + cat_id + "'}";
    //    var tmp_new_name = "";
    //    var tmp_old_name = "";
    //    var filter = _(unsorted_filter).sortBy(function (obj) { return obj.name })
    //    //console.log(filter);
    //    //filter.sort(mySorting);
    //    //alert (filter[0].name + "-" + filter[1].name + "-" + filter[2].name + "-" + filter[3].name);
    //    for (i = 0; i < filter.length; i++) {
    //        tmp_new_name = filter[i].name;
    //        if (tmp_new_name != tmp_old_name) {
    //            // new filter name
    //            if (tmp_old_name == "") {
    //                // first time
    //                qs = qs + ",'feature." + filter[i].name + "':{$in :['" + filter[i].value + "'";
    //            }
    //            else {
    //                qs = qs + "]},'feature." + filter[i].name + "':{$in :['" + filter[i].value + "'";
    //            }
    //        }
    //        else {
    //            if (tmp_old_name == "") {
    //                // first time
    //                qs = qs + "','feature." + filter[i].name + "':$in :{['" + filter[i].value + "'";
    //            }
    //            else {
    //                qs = qs + ",'" + filter[i].value + "'";
    //            }
    //        }
    //        // qs = qs+",'feature."+ filter[i].name + "':'" +filter[i].value + "'";
    //        tmp_old_name = filter[i].name;
    //        if (i == filter.length - 1) { // reached to end
    //            qs = qs + "]";
    //        }
    //    }
    //    qs = qs + "}}";

    //    console.log(qs);
    //    //qs="{cat_id: {'id': '1.1.1'}}";
    //    //qs="{cat_id: {'id': '1.1.1'},'feature.Color':{$in :['Blue']}}";
    //    //console.log(qs);
    //    var apiPath = cat_service_url + '/store/prod_list_by_qs?json=true';

    //    data = qs;
    //    var deferred = $q.defer();
    //    $http({
    //        method: 'POST',
    //        url: apiPath,
    //        data: data,
    //        type: JSON
    //    }).success(function (data) {
    //        deferred.resolve(data);
    //    }).error(function (data) {
    //        deferred.reject("An error occured while validating User");
    //    })

    //    return deferred.promise;
    //};

});