/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 1/7/13
 * Time: 8:20 PM
 * To change this template use File | Settings | File Templates.
 */

app.service('cartService', function (localStorageService, utilService) {

    var express_shipping;
    if  (localStorageService.get('express_shipping') != null)
    {
        express_shipping  =localStorageService.get('express_shipping');
    }
    else {
        express_shipping  = 0;
    }

//    return {
//        getexpress_shipping: function () {
//            return express_shipping;
//        },
//        setexpress_shipping: function(value) {
//            express_shipping = value;
//        }
//    }
    //localStorageService.add('local_cart',cartService.get_Cart());
    this.load_cart_from_storage = function () {

        if ((cart.length == 1) || (cart.length ==0)) {
            if  (localStorageService.get('local_cart') != null)
            {
                cart =localStorageService.get('local_cart');
            }
        }
    }
    this.get_Cart = function () {

      
        if ((cart.length == 1) || (cart.length ==0))

        {
            if ((cart.length ==1) && (typeof cart[0].id === 'undefined')){
                //console.log("inside");
                // for first time browser load assuming cart is stored in localstorage
                this.load_cart_from_storage();
            }
        }
        //console.log(cart);
        if (cart != null) {
            if ((cart[0] != null) && (cart[0].id == null)){
                cart.splice(0,1);
            }
        }
        return  cart;
    };
    this.get_cart_amount = function () {

      
        var tmp = this.get_Cart();
        var total_amount ;
        var min_shipping = utilService.get_min_shipping();
        var shipping_charge = utilService.get_shipping_charge();
        var shipping = 0;
        var total_amount_billable;
        var total_amount_final_ord = 0;
        for (i = 0; i < tmp.length; i++) {
            total_amount_final_ord = total_amount_final_ord + tmp[i].final_offer * tmp[i].quantity;
        }

        if (total_amount_final_ord < min_shipping) {
            shipping  = shipping_charge;
        }
        else{
            shipping = 0;
        }
        total_amount_billable = total_amount_final_ord + shipping ;
        return total_amount_billable;
    };
    this.get_Cart_Size = function () {

        if ((cart.length == 1) || (cart.length == 0)) {
            this.load_cart_from_storage();
        }
        var size ;
        size = 0;
        for (i=1; i<cart.length; i++){
            size = size + cart[i].quantity;
        }
        return  size;
    };
    this.get_Cart_Item_Size = function () {
        
        var cart_size=0;
        for (i=0; i<cart.length; i++){
            cart_size = cart_size + parseInt(cart[i].quantity);
        }
        return  cart_size;
    };
    this.update_to_new_cart = function (new_cart) {
        
        localStorageService.add('local_cart',new_cart);
    }
    this.update_cart_item_size = function (cart_item_id, new_sku) {

        for (i=0; i<cart.length; i++){
            if (cart[i].cart_item_id== cart_item_id){
                cart[i].sku =new_sku;
                break;
            }
        }
        localStorageService.add('local_cart',cart);
    }

    this.load_cart = function (cart_item){

        cart.push({
            cart_item_id :  cart_item.sku,
            id: cart_item._id,
            sku: cart_item.sku,
            brand: cart_item.brand,
            quantity : cart_item.quantity,
            mrp: cart_item.mrp,
            final_offer:cart_item.final_offer,
            name: cart_item.Name,
            image: cart_item.image_urls[0]   ,
            discount: cart_item.discount,
            sizes: cart_item.sizes,
            express: cart_item.express
        });
        localStorageService.add('local_cart',cart);
    }


    this.addTocart = function (product, selected_child_sku, sizes) {

        // selected_child_sku is passed only when there is child product
        // otherwise we assume no child prod and we deal with parent prod only
        var found;
        found = false;
        for (i=0; i<cart.length; i++){
            if ((selected_child_sku!=null) && (selected_child_sku!="")) {       // only when there is child product
                if (cart[i].sku == selected_child_sku.sku){
                    cart[i].quantity =cart[i].quantity +1;
                    found = true;
                }
            }
            else{
                if (cart[i].id == product.id){
                    cart[i].quantity =cart[i].quantity +1;
                    found = true;
                }
            }
        }

        // ensure price part of the product structure is an object and not a string
        var product_price = null;
        if ((typeof product.price) != "object"){
            product_price = JSON.parse(product.price);
        }
        else{
            product_price = product.price;
        }

        // set express
        var express;
        if (product.Express==null){
            express = false;
        }
        else if (product.Express==0){
            express = false;
        }
        else {
            express = true;
        }
        //alert (localStorageService.get("user_info").email_id)    ;
        if ((selected_child_sku!=null) && (selected_child_sku!="")) {       // only when there is child product
            if (!found){
                cart.push({
                    //id: selected_child_sku.id,
                    //cart_item_id : selected_child_sku.sku,
                    cart_item_id : selected_child_sku.sku,
                    id: product.id,
//                    _id: cart_item._id,
                    sku: selected_child_sku.sku,
                    brand: product.brand,
                    quantity : 1,
                    mrp: product_price.mrp,
                    final_offer:product_price.final_offer,
                    name: product.Name,
                    image: product.image_urls[0],
                    discount: (((product_price.mrp - product_price.final_offer)/product_price.mrp) * 100).toFixed(0),
                    sizes: sizes,
                    selected_size:selected_child_sku.size,
                    express: express
                });

            }
        }
        else{
            if (!found){
                cart.push({
                    cart_item_id :  product.sku,
                    id: product.id,
//                    _id: cart_item._id,
                    sku: product.sku,
                    brand: product.brand,
                    quantity : 1,
                    mrp: product_price.mrp,
                    final_offer:product_price.final_offer,
                    name: product.Name,
                    image: product.image_urls[0]   ,
                    discount: (((product_price.mrp - product_price.final_offer)/product_price.mrp) * 100).toFixed(0),
                    sizes: sizes,
                    selected_size: "ONESIZE",
                    express: express
                });

            }
        }
        localStorageService.add('local_cart',cart);
        //this.save_cart_to_db();
        //alert (localStorageService.get('local_cart')) ;
    };

    this.addTocart_from_wishlist = function (product, selected_child_sku, sizes,images) {

        // selected_child_sku is passed only when there is child product
        // otherwise we assume no child prod and we deal with parent prod only
        var found;
        found = false;
        for (i=0; i<cart.length; i++){
            if ((selected_child_sku!=null) && (selected_child_sku!="")) {       // only when there is child product
                if (cart[i].sku == selected_child_sku.sku){
                    cart[i].quantity =cart[i].quantity +1;
                    found = true;
                }
            }
            else{
                if (cart[i].id == product.id){
                    cart[i].quantity =cart[i].quantity +1;
                    found = true;
                }
            }
        }

        // ensure price part of the product structure is an object and not a string
        var product_price = null;
        if ((typeof product.price) != "object"){
            product_price = JSON.parse(product.price);
        }
        else{
            product_price = product.price;
        }

        // set express
        var express;
        if (product.Express==null){
            express = false;
        }
        else if (product.Express==0){
            express = false;
        }
        else {
            express = true;
        }
        //alert (localStorageService.get("user_info").email_id)    ;
        if ((selected_child_sku!=null) && (selected_child_sku!="")) {       // only when there is child product
            if (!found){
                cart.push({
                    //id: selected_child_sku.id,
                    //cart_item_id : selected_child_sku.sku,
                    cart_item_id : selected_child_sku.sku,
                    id: product.id,
//                    _id: cart_item._id,
                    sku: selected_child_sku.sku,
                    brand: product.brand,
                    quantity : 1,
                    mrp: product_price.mrp,
                    final_offer:product_price.final_offer,
                    name: product.Name,
                    image: images,
                    discount: (((product_price.mrp - product_price.final_offer)/product_price.mrp) * 100).toFixed(0),
                    sizes: sizes,
                    selected_size:selected_child_sku.size,
                    express: express
                });

            }
        }
        else{
            if (!found){
                cart.push({
                    cart_item_id :  product.sku,
                    id: product.id,
//                    _id: cart_item._id,
                    sku: product.sku,
                    brand: product.brand,
                    quantity : 1,
                    mrp: product_price.mrp,
                    final_offer:product_price.final_offer,
                    name: product.Name,
                    image: images   ,
                    discount: (((product_price.mrp - product_price.final_offer)/product_price.mrp) * 100).toFixed(0),
                    sizes: sizes,
                    selected_size: "ONESIZE",
                    express: express
                });

            }
        }
        localStorageService.add('local_cart',cart);
        //this.save_cart_to_db();
        //alert (localStorageService.get('local_cart')) ;
    };



    this.update_cart = function (){
        console.log(cart);
        localStorageService.add('local_cart',cart);
    }

    this.delete_from_cart = function(id){
        for (i=0; i<cart.length; i++){
            if (cart[i].id == id){
                cart.splice(i,1);
            }
        }
        localStorageService.add('local_cart',cart);
    }

    this.save_cart_to_db = function($http, $q){
        var server_cart= {};
        var e = localStorageService.get('user_info');
        var u;
        if (e.hasOwnProperty('email_id')){
            u = e.email_id;
        }
        else {
            u= e.user_id;
        }
        server_cart.email_id = u;
        server_cart.cart_items =  cart;
        var apiPath = cat_service_url + '/store/cart/write_cart/';
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: apiPath,
            data: server_cart,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
                //localStorageService.add('user_info', data[0]);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }




    var cart =[
        {
//            prod_id: 'ABCDFER1', quantity: 2, unit_price: 1400.5,  price:2801.00
        }

    ];



//    this.add_cart = function ($http, $q, dataobj) {
//
//        var apiPath = 'http://localhost:8181/checkout/cart/';
//        var deferred = $q.defer();
//        $http({
//            method: 'POST',
//            url: apiPath,
//            data: dataobj,
//            type: JSON
//        }).success(function (data) {
//            deferred.resolve(data);
//            localStorageService.add('cart_id', data);
//        }).error(function (data) {
//            deferred.reject("An error occured while validating User");
//        })
//        return deferred.promise;
//
//    };


    this.get_cart_id = function () {

        var cart_id = '';
        cart_id = localStorageService.get('cart_id');
        return cart_id;
    }


});