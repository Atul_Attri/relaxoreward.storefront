﻿app.service('checkoutService', function (localStorageService) {
 
    this.addShippingInfo = function ($http, $q, dataobj) {

        localStorageService.add('shipping_info', dataobj);

        var apiPath = cat_service_url + "/checkout/cartshipping/";
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("An error occured while validating User");
        })
        return deferred.promise;
    };

    this.get_shipping_info = function () {
        return localStorageService.get('shipping_info');
    }


});

