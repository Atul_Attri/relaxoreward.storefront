﻿
app.service('contactusService', function (localStorageService) {


    this.SaveContactUs = function ($http, $q, dataobj) {

       

        var apiPath = cat_service_url + '/user/registration/postContactUs/';
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("An error occured while Saving Data");
        })
        return deferred.promise;
    };

});