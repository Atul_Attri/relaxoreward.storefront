/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 19/9/13
 * Time: 5:22 AM
 * To change this template use File | Settings | File Templates.
 */

app.service('expressService', function () {

    this.get_filters = function ($http, $q, prem){
        var apiPath = cat_service_url + '/store/express_items_filters?json=true';

        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

        return deferred.promise;
    };

    this.get_express_prod_list = function ($http, $q, prem, min, max){
        //var apiPath = 'http://localhost:8181/store/prod_list_by_cat_short?cat_id=' + cat_id + '&json=true';
        var apiPath = cat_service_url + '/store/express_prod_list?min=' + min + '&max=' + max + '&json=true';
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            //data: data,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

        return deferred.promise;
    };


});