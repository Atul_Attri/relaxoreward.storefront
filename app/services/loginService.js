﻿app.service('loginService', function (localStorageService) {
    this.IsValidUser = function ($http, $q, dataobj) {
        var apiPath = cat_service_url + '/user/registration/user_login/';
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
            localStorageService.add('user_info', data[0]);
        }).error(function (data)
        {
            deferred.reject("An error occured while validating User");
        })
        return deferred.promise;
    };

    this.get_cart_by_email = function ($http, $q, email_id) {
        var apiPath = cat_service_url + "/store/cart?email_id=" + email_id + "&json=true";
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;
    };

   
    //this.get_wishlist_by_email = function ($http, $q, email_id) {

    //    var apiPath = cat_service_url + "/store/wishlist?email_id=" + email_id + "&json=true";
    //    var deferred = $q.defer();

    //    $http({
    //        method: 'GET',
    //        url: apiPath,
    //        type: JSON
    //    }).success(function (data) {
    //        deferred.resolve(data);
    //    }).error(function (data) {
    //        deferred.reject("An error occured while validating User");
    //    })
    //    return deferred.promise;
    //};


       
});