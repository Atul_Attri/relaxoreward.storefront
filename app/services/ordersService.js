﻿/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 1/7/13
 * Time: 8:20 PM
 * To change this template use File | Settings | File Templates.
 */

app.service('orderService', function (localStorageService) {
    //localStorageService.add('local_cart',cartService.get_Cart());
    this.load_cart_from_storage = function () {
        if (cart.length == 1) {
            if (localStorageService.get('local_cart') != null) {
                cart = localStorageService.get('local_cart');
            }
        }
    }
    this.get_Cart = function () {
        //alert (cart);
        if (cart.length == 1) {
            this.load_cart_from_storage();
        }
        if (cart[0].id == null) {
            cart.splice(0, 1);
        }
        return cart;
    };

    this.get_Cart_Size = function () {

        if (cart.length == 1) {
            this.load_cart_from_storage();
        }
        var size;
        size = 0;
        for (i = 1; i < cart.length; i++) {
            size = size + cart[i].quantity;
        }
        return size;
    };

    this.addTocart = function (product) {
        var found;
        found = false;
        for (i = 0; i < cart.length - 1; i++) {
            if (cart[i].id == product.id) {
                cart[i].quantity = cart[i].quantity + 1;
                found = true;
            }
        }
        if (!found) {
            cart.push({
                id: product.id,
                sku: product.sku,
                brand: product.brand,
                quantity: 1,
                mrp: product.price.mrp,
                final_offer: product.price.final_offer,
                name: product.Name,
                image: product.image
            });

        }
        //        alert (cart.length);
        localStorageService.add('local_cart', cart);
    };

    this.update_cart = function (id, qty) {
        localStorageService.add('local_cart', cart);
    }

    this.delete_from_cart = function (id) {
        for (i = 0; i < cart.length - 1; i++) {
            if (cart[i].id == id) {
                cart.splice(i, 1);
            }
        }
        localStorageService.add('local_cart', cart);
    }

    var cart = [
        {
            //            prod_id: 'ABCDFER1', quantity: 2, unit_price: 1400.5,  price:2801.00
        }

    ];

    var cart_id = '';

    this.Payment = function ($http, $q, dataobj) {

        var apiPath = 'http://localhost:8181/checkout/order/';
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
         
        }).error(function (data) {
            deferred.reject("An error occured while validating User");
        })
        return deferred.promise;

    };

    
});

