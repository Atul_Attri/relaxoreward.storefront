
app.service('registrationService', function () {
    this.UserRegistration = function ($http, $q, dataobj) {
        //var apiPath = 'http://localhost:7898/user/registration/submit_registration/';
        //var apiPath = 'http://driplqc.cocib.net/user/registration/submit_registration/';
        var apiPath = cat_service_url + '/user/registration/submit_registration/';
        //alert (apiPath);
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("An error occured while validating User");
        })
        return deferred.promise;

    };

    this.ResendActCode = function ($http, $q, dataobj) {

        var apiPath = cat_service_url + '/user/registration/ResendActCode/';
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject("An error occured while validating User");
        })
        return deferred.promise;

    };
});