/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 6/10/13
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */
//This is in checkout app
app.controller('CatController', function ($scope, $location, $window, $routeParams) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below

    function init() {
        $window.location.href = root_url + "/index.html#/cat/" +  $routeParams.catId;
    }
    init();

});