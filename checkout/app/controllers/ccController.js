/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/10/13
 * Time: 11:15 AM
 * To change this template use File | Settings | File Templates.
 */

app.controller('CcController', function ($scope, $http, $q, $location, $window,
                                         $routeParams, ccService,localStorageService, cartService ) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below

  

    function check_login (){
        if (localStorageService.get('user_info') == null){
            $window.location.href = root_url + "/index.html#/login";
        }
        else
        {
            $window.location.href = root_url + "/home.html";
        }
    }
    $scope.select_address = function(selected_address){
        populate_selected_address(selected_address);
    }
    var get_user_shipping = function(){
        shipService.get_user_shipping_list($http, $q, localStorageService.get('user_info').email_id).then(function(data){
                $scope.user_shipping_list =  data;
                //console.log($scope.user_shipping_list) ;
            },
            function(){
                //Display an error message
                $scope.error= error;
            });
    }
    $scope.save_data = function(){

        // TODO - validate data entry

        var ship_data = [];
        ship_data.push(cartService.get_Cart());
        ship_data.push(selected_address);
        shipService.save_shipping_page($http, $q , selected_address).then (function(data){
            //alert("sss");
        });
    }
    var populate_selected_address = function (address){
        selected_address.id = address.id;
        selected_address.name = address.name;
        selected_address.shipping_name = address.shipping_name;
        selected_address.address= address.address;
        selected_address.city= address.city;
        selected_address.state = address.state;
        selected_address.pincode = address.pincode;
        selected_address.mobile_number= address.mobile_number;
        selected_address.user_id = localStorageService.get('user_info').email_id;
        selected_address.store = store_signature;
        $scope.selected_address = selected_address;
    }
    var order_id;
    function get_order_id(){

    }
    function init() {
        // get order_id
        order_id = $routeParams.order_id;

        // get selected address
        $scope.selected_address = localStorageService.get('selected_ship_address');

       // alert($scope.selected_address.shipping_name);

        $scope.cc = {};

        $scope.cc.total_amount = cartService.get_cart_amount();
        $scope.cc.Order_Id = $routeParams.order_id;
        //$scope.cc.Redirect_Url =
        $scope.cc.billing_cust_name = $scope.selected_address.name;
        $scope.cc.billing_cust_address = $scope.selected_address.shipping_name;
        //$scope.cc.billing_cust_country =
        $scope.cc.billing_cust_state = $scope.selected_address.state;
        $scope.cc.billing_cust_tel = $scope.selected_address.mobile_number;
        $scope.cc.billing_cust_email = localStorageService.get('user_info').email_id;
        $scope.cc.name = $scope.selected_address.name;
        $scope.cc.address = $scope.selected_address.shipping_name;
        //$scope.cc.delivery_cust_country =
        $scope.cc.state = $scope.selected_address.state;
        $scope.cc.mobile_number = $scope.selected_address.mobile_number;
        //$scope.cc.delivery_cust_notes =
        //$scope.cc.Merchant_Param =
        $scope.cc.billing_cust_city = $scope.selected_address.city;
        $scope.cc.billing_zip_code = $scope.selected_address.pincode;
        $scope.cc.delivery_cust_city = $scope.selected_address.city;
        //$scope.cc.delivery_zip_code =


        console.log(cartService.get_cart_amount());
    }
    $scope.route_to = function (page){
        var url = "/" + page + "/" + order_id;
        $location.path(url);
    }
    var selected_address = {};
    init();

});