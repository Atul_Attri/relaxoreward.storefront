/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 2/12/13
 * Time: 6:20 AM
 * To change this template use File | Settings | File Templates.
 */
app.controller('express_popupController', function ($scope, $http, $q, $location, $window,  $routeParams,  $rootScope,
                                           express_popupService, localStorageService, cartService, utilService ) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    function check_login (){
        if (localStorageService.get('user_info') == null){
            $window.location.href = root_url + "/index.html#/login";
        }
        else
        {
            $window.location.href = root_url + "/home.html";
        }
    }
    // get order_id
    order_id = $routeParams.order_id;

    $scope.proceed_express = function(){
        //cartService.express_shipping = true;
        //$rootScope.express_shipping = 150;   //todo config
        localStorageService.add("express_shipping", 150);
        $scope.save_express_shipping_data();
        $location.path("/pp/" + order_id);
    }

    $scope.proceed_normal = function(){
        //cartService.express_shipping = false;
        //$rootScope.express_shipping = 0;
        localStorageService.add("express_shipping", 0);
        $location.path("/pp/" + order_id);
    }


    $scope.save_express_shipping_data = function(){
        express_popupService.add_express_shipping($http, $q, order_id, 150).then(function(data){       // todo config
                //Update UI using data or use the data to call another service
                o_id  = data;
            },
            function(){
                //Display an error message
                $scope.error= error;
            });


    }

    function init() {
        //check_login();
        // set express shipping to zero as default
        localStorageService.set ("express_shipping", 0);
    }
    var selected_address = {};
    init();

});
