/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/10/13
 * Time: 11:17 AM
 * To change this template use File | Settings | File Templates.
 */

app.service('ccService', function () {


    this.get_user_shipping_list = function($http, $q, user_id){
        var apiPath = cat_service_url + '/store/user_shipping?email=' + user_id ;
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }
    this.save_shipping_page = function($http, $q, dataobj){
        var apiPath = cat_service_url + '/store/checkout/write_ship_data/';
        console.log(dataobj);
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: apiPath,
            data: dataobj,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
                //localStorageService.add('user_info', data[0]);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }




});