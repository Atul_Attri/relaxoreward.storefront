/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/10/13
 * Time: 1:09 PM
 * To change this template use File | Settings | File Templates.
 */
app.service('ppService', function () {
    this.validate_discount_coupon = function($http, $q, coupon, store, user_id){
        var apiPath = cat_service_url + '/store/coupon/validate?coupon='+ coupon  + "&store=" + store + "&user="+ user_id + "&json=true" ;
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;

    }
    this.RedeemUserPoint = function ($http, $q, dataobj) {


        //var da = $('#myform').serializeArray();/*Get All Form data */

//        var a = dataobj.total_amount;
//
//        if (a <= 0) {

            var apiPath = cat_service_url + '/gv/pay_by_gv_points/';
            var deferred = $q.defer();

            var user_pont = {
                EmailID:dataobj.billing_cust_email,
                OrderID:dataobj.Order_Id,
                PaidAmt:dataobj.total_amount,
                Points: dataobj.Points,
                Points_INR:dataobj.Points_Inr,
                strActualAmt:dataobj.total_amount,
                egiftVouNo:dataobj.EGift_VNO,
                egiftVouAmt: dataobj.EGift_Amt,
                discount_coupon: dataobj.discount_coupon,
                discount_amount: dataobj.discount_coupon_total,
                status:2
            }


            $http({
                method: 'POST',
                url: apiPath,
                data: user_pont,
                type: JSON
            }).success(function (data) {
                    deferred.resolve(data);
//                    var confirm_order_url = root_url + "/payment.html#/payment/2";



            }).error(function (data) {
                deferred.reject("An error occured while Processing Payment");
            })
            return deferred.promise;
//        }
//        else {
//            $('#myform').submit();
//        }



    }


});