/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/7/13
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
var adItems = {
    "landing_page_main_scroller" : [{
        "href" : "#",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }],
    "landing_page_static_html" : "https://s3-ap-southeast-1.amazonaws.com/annectos/ads/men_static_ad.html",
    "offer_items" : [{
        "id" : "001",
        "offer_text" : "Only till 24th July midnight | Additional 10% off on Nike Shoes only for Intel employees",
        "href" : "#/cat/1.0/"
    }, {
        "id" : "002",
        "offer_text" : "Additional 5% off on Women Hand Bags only for Intel employees",
        "href" : "#/cat/2.0/"
    }],
    "l1_banner" : [{
        "id" : "20",
        "href" : "#/cat/1.1.7",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "30",
        "href" : "#/cat/2.1.2",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "50",
        "href" : "#/cat/3.1.4",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "75",
        "href" : "#/cat/4.1.4",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "100",
        "href" : "#/cat/5.1.1",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "150",
        "href" : "#/cat/6.1.4",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "250",
        "href" : "#/cat/7.1.4",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "500",
        "href" : "#/cat/7.1.4",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "750",
        "href" : "#/cat/7.1.4",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "1000",
        "href" : "#/cat/7.1.4",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "1500",
        "href" : "#/cat/7.1.4",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "2000",
        "href" : "#/cat/7.1.4",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }, {
        "id" : "3500",
        "href" : "#/cat/7.1.4",
        "link" : "https://s3.amazonaws.com/relaxo-all/welcome_relaxo.jpg"
    }]
};