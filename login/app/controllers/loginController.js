﻿app.controller('loginController', function ($scope,$http,$q, $window, $location, $rootScope, loginService,
                                            state_managerService, localStorageService, cartService) {


    $scope.$on("login_success", function() {
        // This function listens to any change in scope for broadcast message login_success.
        // If it gets that, applies subsequent code
        set_login_text();




    });
    var set_login_text = function(){
        if  (localStorageService.get('user_info') != null)
        {
            user_info=localStorageService.get('user_info');
            $scope.signin_text = ("welcome " + user_info.first_name + " " + user_info.last_name).toTitleCase() ;
            $scope.signout_text = "Sign Out" ;
        }
        else{
            $scope.signin_text = "Sign In";
            $scope.signout_text = "Sign Up";
        }
    }

    init();


    function init() {
        var user_info;
        set_login_text();
        }

    $scope.opts = {
        backdropFade: true,
        dialogFade:true
    };

    $scope.openRegistration = function(){
        localStorageService.set('user_info', "");
        localStorageService.set('local_cart', "[]");
        state_managerService.refer_url = $location.path();
        var url = root_url + "/login.html#/registration" ;
        console.log(url);
        $window.location.href = url;
    }
    $scope.openLogin = function(){
        localStorageService.set('user_info', "[]");
        localStorageService.set('local_cart', "");
        state_managerService.refer_url = $location.path();
        var login_url = root_url + "/login.html#/login" ;
        $window.location.href = login_url;
    }
    $scope.execLogout = function(){
        var url;
        $window.location.href = login_url;
        if  (localStorageService.get('user_info') != null)
        {
            localStorageService.add('user_info',"");
            this.openLogin();
        }
        else {
            localStorageService.add('user_info',"");
            this.openRegistration();
//            url =  root_url + "/index.html#/registration" ;
//            $window.location.href = url;

        }
    }

       $scope.validateUserService = function () {
            console.log($scope.userlogin);
            if (($scope.userlogin == null) || ($scope.userlogin == "")) {
                alert ("Please enter login information");
            }
           else {
                if  (($scope.userlogin.email_id == null) || ($scope.userlogin.email_id == "")){
                    alert ("Please enter email address to login")
                }
                else{
                    if  (($scope.userlogin.password ==null) || ($scope.userlogin.password =="")) {
                        alert ("Please enter password to login")
                    }
                    else{
                        var returnval = '';

                        var domain = store_email_signature;
                        var Length = domain.length;
                        var email_domain = domain.substring(2, Length);
                        //var regemail = $scope.registration.email_id;
                        var regemail = $scope.userlogin.email_id;
                        var returnval = '';
                        var chk = 0;
                        if (domain == '*.*') {
                            chk = 1;
                        }
                        else {
                            chk = regemail.indexOf(email_domain);
                        }

                        if (chk != -1) {

                            $scope.userlogin.company = store;

                            loginService.IsValidUser($http, $q, $scope.userlogin).then(function (data) {
                                    //Update UI using data or use the data to call another service
                                    //$scope.message = data;
                                    alert(data[0].message);
                                    //console.log(data);
                                    if (data[0].message == "Log In Successfully") {
                                        // now get latest cart
                                        loginService.get_cart_by_email($http, $q, localStorageService.get('user_info').email_id).then(function (data) {
                                                //Update UI using data or use the data to call another service
                                                cartService.cart = data;
                                                localStorageService.add('local_cart', data);
                                                //console.log(cartService.cart);
                                                //console.log(localStorageService.get('local_cart'));

                                                $rootScope.$broadcast('cart_loaded');
                                                $scope.message = "Login Success";
                                                $scope.user_name = $scope.userlogin.email_id;
                                                $rootScope.$broadcast('login_success');
                                                //                               if (state_managerService.refer_url==null){
                                                //                                   state_managerService.refer_url= root_url + "/index.html#/home" ;
                                                //                               }
                                                if (typeof homepage === 'undefined' || homepage == "") 
											    {
                                                     $window.location.href = root_url + "/index.html#/home";
												}
												else 
												{
													if (typeof homeurl === 'undefined' || homeurl == "") {
														$window.location.href = root_url + "/index.html#/home";                                         
													}
													else {
														$window.location.href = root_url + homeurl;
													}
												}
                                                //                               $location.path(state_managerService.refer_url);
                                                $scope.isVisible = true;

                                            },
                                            function () {
                                                //Display an error message
                                                $scope.error = error;
                                            });


                                    }
                                    else {
                                        //$scope.message = data.value;
                                        $scope.message = data[0].message;
                                        $scope.signin_text = "Sign In"
                                        localStorageService.set('user_info', "");
                                    }

                                    localStorageService.set('user_info', data[0]);

                                },
                                function () {
                                    //Display an error message
                                    $scope.error = error;
                                });

                        }
                    }
                }
            }
       //console.log($scope.userlogin);
           //console.log(state_managerService.refer_url);

    };
});