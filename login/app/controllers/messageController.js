﻿
app.controller('MessageController', function ($scope, $routeParams, $location, $http, $q, $sce,  messageService,
                                              registrationService, localStorageService) {

    $scope.showButton = function () {
        return true;
    }
	$scope.showLoginButton = function () {
        return true;
    }


    Valid_Message = localStorageService.get('message');


    if (Valid_Message == "Your Registration Has Been Successfully Completed") {
        //$location.path("#/registration_success");
        //alert("Registration is Successfull now!  Please check your email for activation of account.");
        Valid_Message = "Registration is Successfull now!  Please check your email for activation of account.";
        $scope.message = Valid_Message;
        localStorageService.set("user_info", "");
        localStorageService.set("reg_user", "");
        //$location.path("#/home");
    }
    else if (Valid_Message == "Your Account Activation Is Pending") {
        var msg =   "Your Account Activation Is Pending. " +
                    "<br/> Please check your mailbox for an email from <a href='mailto://customerfirst@annectos.in'> annectoś Sales. </a>" +
                    "<br/> Also check your spam folder if required. " +
                    "<br/> In case you don't find it, please call our customer service number or contact your HR representative" ;
        $scope.message = msg;
        localStorageService.set("user_info", "");

        $scope.showButton = function () {
            return false;
        }
    }
    else if (Valid_Message == "This Email Is Already Registered") {
        var msg =   "Sorry! This email seems to be already registered with us.  " +
                    "<br/> If you have forgotten your password, you can reset by clicking <a href='#/forgot_password'>Forgot Password</a> link" +
                    "<br/> Please <a href='#/login'>Login</a> here." +
                    "<br/>In case you feel there is some mistake or error in the system, please call our customer service number or contact your HR representative" ;
        $scope.message = msg;
        localStorageService.set("user_info", "");

        $scope.showLoginButton = function () {
            return false;
        }


    }
	else if (Valid_Message == "Your Password Has Been Changed Successfully") {
        
        $scope.message = Valid_Message;
        localStorageService.set("user_info", "");

        $scope.showLoginButton = function () {
            return false;
        }


    }
	else if (Valid_Message == "You Can Not Reset Your Password With This Link") {
        
        $scope.message = Valid_Message;
        localStorageService.set("user_info", "");

    }
	
    else {
        localStorageService.set("reg_user", "");
        $scope.message = Valid_Message;
    }

    $scope.trustedMessage = $sce.trustAsHtml($scope.message);

    $scope.SendLogin = function () {
     
        $location.path("/login");
    }

    $scope.ResendActCode = function () {

        Reg_User_Info = localStorageService.get('reg_user');

        var returnval = '';
        //returnval = registrationService.UserRegistration($http, $q, $scope.registration);
        registrationService.ResendActCode($http, $q, Reg_User_Info).then(function (data) {
            console.log(data + " -- " + data.toString().toLowerCase() + " -- " + "success");

            if (JSON.parse(data.toString()) == "Invalid Email") {
                $scope.message = JSON.parse(data.toString());
            }
            else {
                $scope.message = JSON.parse(data.toString());
                $scope.showButton = function () {
                    return true;
                }
            }
            $scope.trustedMessage = $sce.trustAsHtml($scope.message);

        },
            function () {
                //Display an error message
                $scope.error = error;
            });
    };


});
