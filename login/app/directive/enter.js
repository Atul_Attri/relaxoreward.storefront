/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/11/13
 * Time: 7:19 AM
 * To change this template use File | Settings | File Templates.
 */
app.directive('handleEnter', function() {
    return function(scope, element, attrs) {
        //alert (attrs);
        element.bind("keydown keypress", function(event) {
            if(event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});