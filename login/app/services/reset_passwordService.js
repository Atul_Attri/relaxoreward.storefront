﻿app.service('reset_passwordService', function () {
   
        this.resetPwdCheck = function ($http, $q, dataobj) {
            var apiPath = cat_service_url + '/user/registration/checkEmailGuid/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                    deferred.resolve(data);
                    //localStorageService.add('user_info', data[0]);
                }).error(function (data)
                {
                    deferred.reject("An error occured while validating User");
                })
            return deferred.promise;
        };
		
		
		this.SaveNewPassword = function ($http, $q, dataobj) {
            var apiPath = cat_service_url + '/user/registration/SaveNewPassword/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                    deferred.resolve(data);
                    //localStorageService.add('user_info', data[0]);
                }).error(function (data)
                {
                    deferred.reject("An error occured while validating User");
                })
            return deferred.promise;
        };
    });





   